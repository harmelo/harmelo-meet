@extends('app.master')

@section('content')
<harmelo-meet :events="{{json_encode($events)}}" :user="{{json_encode($user)}}"></harmelo-meet>
@endsection