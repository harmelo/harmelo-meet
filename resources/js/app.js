require('./bootstrap');

import Vue from 'vue';

// axios
import axios from 'axios';
import VueAxios from 'vue-axios';


import 'material-icons/iconfont/material-icons.css';

window.axios = require('axios');
Vue.prototype.$http = window.axios;
Vue.use(VueAxios, axios);

window.Vue = Vue;
window.Vue.config.productionTip = false;

Vue.component('harmelo-meet', require('./modules/meet/Meet.vue').default);

var app = new Vue({
    el: '#app'
});
