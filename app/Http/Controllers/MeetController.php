<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class MeetController extends Controller
{
    public function meet(Request $request) {
        $q = $request->q;
        $id = $request->id;
        $code = $request->code;
        $ref = $request->ref;
        $user_id = $request->cid;

        $exists = DB::table('tbl_auto_login')->where('code', $code)->first();
        if (empty($exists))
                abort(404);

        $exists = DB::table('tbl_meeting_code')->where('code', $ref)->first();
        if (empty($exists))
            abort(404);

        $events =  DB::table('tbl_events')->where('id', $id)->first();

        if (empty($events))
            abort(404);

        $user =  DB::table('users')->where('id', $user_id)->first();

        if (empty($user))
            abort(404);
    
        return view('meet.index', [
            'event_name' => $q,
            'event_id' => $id,
            'code' => $code,
            'ref' => $ref,
            'events' => $events,
            'user' => $user
        ]);
    }
}
